Requirements
============


Android SDK tools
-----------------
Download the command line tools:

.. code-block:: bash

    wget -c https://bitbucket.org/radiantforandroid/radiantframework/downloads/tools_r25.2.5-linux.zip
    unzip tools_r25.2.5-linux.zip -d android_sdk
    cd android_sdk/tools/bin
    chmod a+x sdkmanager
    ./sdkmanager "platforms;android-26"
    ./sdkmanager "platform-tools"
    ./sdkmanager "build-tools;26.0.2"

We are using the 25.2.5 release because the current compiler use ``Ant`` instead
of ``Gradle``, now you must have ``android_sdk/platforms/android-26`` in your
system, take note about `android_sdk` absolute path.


Crystax NDK
-----------
We are using **Python3**, so, we need
`Crystax NDK <https://www.crystax.net/en/download>`_ and NOT the official.
`[python-for-android now supports Python 3 APKs] <https://kivy.org/planet/2016/01/python-for-android-now-supports-python-3%C2%A0apks/>`_.
Download it and take note about their absolute path.


.. code-block:: bash

    tar xf crystax-ndk-10.3.2-linux-x86_64.tar.xz
    ln -s android-21 crystax-ndk-10.3.2/platforms/android-26





Others
------

 * Cython
 * Gcc
 * Python35

.. code-block:: bash

    pacman -S cython gcc
    yaourt -S python35

Installation
============
We need install the main python module, thats easy, just with:

.. code-block:: python

    pip install radiant


Configuration
=============

Installed apps
--------------
Add **radiant.builder** and **radiant.framework** to your **INSTALLED_APPS** setting.

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'static_precompiler',

        'radiant.builder',
        'radiant.framework',
    )


Static files
------------
Radiant must run internally the ``collectstatic`` and ``compilestatic`` (note static_precompiler dependency)
commands, thats mean that ``STATIC_ROOT`` must be defined.

.. code-block:: python

    STATIC_ROOT = os.path.join(BASE_DIR, 'apk')

Remember edit your statics files too.

.. code-block:: python

    STATICFILES_DIRS = [
        os.path.join(BASE_DIR, "static"),
    ]


Urls
----
Add this to urls.py configuration for extra features like URLs handlers.

.. code-block:: python

    path('rdnt', include('radiant.framework.urls')),

We are usig the development server in production too, `static files <https://docs.djangoproject.com/en/1.9/howto/static-files/#serving-static-files-during-development>`_
can be included with:

.. code-block:: python

    from django.conf import settings
    from django.conf.urls.static import static

    urlpatterns = [
        # ... the rest of your URLconf goes here ...
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


Android configuration
---------------------
The main apk settings are defined in this section.

.. code-block:: python

    ANDROID = {
        'APK': {
            'name': "appname",
            'version': '0.1', #https://developer.android.com/studio/publish/versioning.html
            'numericversion': '1',
            'package': 'com.radiant.appname',
            'icon': os.path.join(STATIC_ROOT, 'images', 'icon.png'),
            'statusbarcolor': '#000000',
            'navigationbarcolor': '#000000',
            'orientation': 'portrait', #other options: 'sensor' and 'landscape'
            'intent_filters': None,
        },

        'APP': {
            'multithread': False,
            'logs': 'logs',  #directory for save logs, by default is in the app folder
        },

        'BRYTHON': False,

        'ANDROID': {
            'ARCH': 'armeabi-v7a',
            'SDK': '/absolute/path/to/android-sdk-linux', #https://developer.android.com/studio/index.html
            'API': '26',
            'CRYSTAX_NDK': '/absolute/path/to/crystax-ndk-10.3.2', #https://www.crystax.net/en/download
            'CRYSTAX_NDK_VERSION': '10.3.2',
            'BUILD_TOOL': 'ant',  #ant, gradle
        },

        #for sign and release packages
        'KEY': {
            'RELEASE_KEYSTORE': os.path.join(BASE_DIR, 'radiant.keystore'),
            'RELEASE_KEYALIAS': 'radiant',
            'RELEASE_KEYSTORE_PASSWD': 'radiant', #use your own password
            'RELEASE_KEYALIAS_PASSWD': 'radiant',
        },

        #splash screen for your app, this is static html, NOT a Django view
        'SPLASH': {
            'static_html': False, #path to .html, resources must be added with just file name, i.e background-image: url("splash.png");
            'resources': [], #list of files used in the static html, i.e ['static/images/splash.png']
        },

        #for localserver
        'PORT': '8888',
        'IP': '127.0.0.1',

        #extra permissions for app https://developer.android.com/reference/android/Manifest.permission.html
        'PERMISSIONS': [], #list of permissions, i.e ['BLUETOOTH', 'BLUETOOTH_ADMIN', 'CAMERA']

        #sandbox for python-for-andoid operations
        'BUILD': {
            'build': '~/.radiant', #where the magic happens
            'recipes': None, #path for user recipes parent directory, check http://python-for-android.readthedocs.io/en/latest/recipes/
            'whitelist': None, #for python-for-android users
            'requirements': [], #extra python packages to install, differents to ['python3crystax', 'pyjnius', 'django', 'sqlite3', 'djangoforandroid']
            'exclude_dirs': [], #list of RELATIVE paths, this directories will not be included in the final .apk
            'include_exts': [], #list of extensions to include in final .apk, i.e ['py', 'png', 'sqlite3', 'html', 'css', 'js'], if empty then add all files
            },

        #Theme
        'THEME': {
            'colors':False, #the xml color pallette, can be generated from https://material.io/tools/color/
        },

    }
