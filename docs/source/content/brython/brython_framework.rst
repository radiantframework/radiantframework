=================
Brython framework
=================

The Brython Framework is the main feature of Radiant module, is based in
`Brython <http://brython.info/>`_ a Python implementation for browsers, with this
tool is possible to do a lot of amazing things, create a `html <http://brython.info/static_doc/en/html.html>`_
application can be really easy.

During development this is a Django application, but after generate the APK and into Android it will not be anymore.

-------------
Start Project
-------------

We can begin with a default template, created with ``startproject`` command,
in this case the project name is ``PitonWeb`` and the application name is ``pitonweb``

.. code-block:: bash

    $ radiant startproject brython PitonWeb 'pitonweb'

Play attention to django app called ``android``, all directories functions will be described below:



android/core
============
Contains the file ``main.py`` with a module called ``AndroidMain`` this module will be intepreted like real
Python **not Brython**, is the connection with the Phone features (like camera, files, etc..).

This must be defined too with the section ``BRYTHON`` in the :ref:`Android configuration`, like this:

.. code-block:: python

    ANDROID = {
    ...

        'BRYTHON': {
            'module': os.path.join(BASE_DIR, 'android', 'core'),
            'class': 'AndroidMain',
        },

    ...
    }

(look at ``settings.py`` file).


android/static
==============
A placeholder for static files.


android/templates
=================
Here is created the splash file for the APP.


android/views
=============
This is the main Brython app, the ``base.py`` is a file that handle the html generation,
this could be as simple as:

.. code-block:: python

    from browser import document, html
    document <= html.H1('Hola mundo!!!')

But there is a complete example using the Bryhon Framework for generate applications with
`Material Components for Web <https://material.io/develop/web/>`_ a complete
guide is in :ref:`Brython MDC Framework` and :ref:`Brython Radiant Framework`

--------
Themming
--------
The color pallette can be created with the `Color Tool <https://material.io/tools/color/>`_ online application.

.. code-block:: xml

    <!--?xml version="1.0" encoding="UTF-8"?-->
    <resources>
      <color name="primaryColor">#4db6ac</color>
      <color name="primaryLightColor">#82e9de</color>
      <color name="primaryDarkColor">#00867d</color>
      <color name="secondaryColor">#ffab00</color>
      <color name="secondaryLightColor">#ffdd4b</color>
      <color name="secondaryDarkColor">#c67c00</color>
      <color name="primaryTextColor">#ffffff</color>
      <color name="secondaryTextColor">#ffffff</color>
    </resources>

----------
Compilling
----------

The complete guide for this and other commands is in :ref:`Django Aplication Commands`


Test application
================

.. code-block:: bash

    $ python manage.py androidapk --brython_test



Generate APK
============

.. code-block:: bash

    $ python manage.py androidapk --debug




