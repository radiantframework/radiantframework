=================
Brython autobuild
=================

The `autobuild` is command line feature that allows the generation of ``.apk`` from a
minimalist code, like `this <https://bitbucket.org/radiantforandroid/autobuild/src/default/>`_,
note the directory structure, is very important.

.. code-block:: bash

    hg clone ssh://hg@bitbucket.org/radiantforandroid/autobuild
    cd autobuild
    radiant autobuild debug create


The options ``debug`` mean that the apk must be generated in debug mode, the other
mode available is ``release``.

The options ``create`` mean that this is the first time, and must download dependencies.



From scratch
============

