-------
Buttons
-------


MDCButton
=========

`MDCButton` element must be imported from `mdcframework.mdc`

.. brython::
    :hide-output:

    from mdcframework.mdc import MDCButton


.. include:: examples/MDCButton.rst



MDCFab
======


`MDCFab` element must be imported from `mdcframework.mdc`

.. brython::
    :hide-output:

    from mdcframework.mdc import MDCFab

.. include:: examples/MDCFab.rst



MDCIconToggle
=============


`MDCIconToggle` element must be imported from `mdcframework.mdc`

.. brython::
    :hide-output:

    from mdcframework.mdc import MDCIconToggle

.. include:: examples/MDCIconToggle.rst

