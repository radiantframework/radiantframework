Floating action button
----------------------

.. brython::

    from mdcframework.mdc import MDCFab
    #!ignore
    
    button = MDCFab('favorite')
    container <= button
            

Floating action button mini
---------------------------

.. brython::

    from mdcframework.mdc import MDCFab
    #!ignore
    
    button = MDCFab('favorite', mini=True)
    container <= button
            
