Icon toggle buttons 
--------------------

.. brython::

    from mdcframework.mdc import MDCIconToggle
    #!ignore
    
    button = MDCIconToggle('favorite', 'favorite_border')
    container <= button
    
    button = MDCIconToggle('star', 'star_border')
    button.mdc.theme('primary')
    container <= button
    
    button = MDCIconToggle('bookmark', 'bookmark_border')
    button.mdc.theme('secondary')
    container <= button
            
