Button
------

.. brython::

    from mdcframework.mdc import MDCButton
    #!ignore
    
    button = MDCButton('Button', raised=False)
    container <= button
            

Raised button
-------------

.. brython::

    from mdcframework.mdc import MDCButton
    #!ignore
    
    button = MDCButton('Raised Button', raised=True)
    container <= button
            

Disabled button
---------------

.. brython::

    from mdcframework.mdc import MDCButton
    #!ignore
    
    button = MDCButton('Disabled button', raised=True, disabled=True)
    container <= button
            

Icon button
-----------

.. brython::

    from mdcframework.mdc import MDCButton
    #!ignore
    
    button = MDCButton('Favorite', icon='favorite', raised=True)
    container <= button
            

Icon button reversed
--------------------

.. brython::

    from mdcframework.mdc import MDCButton
    #!ignore
    
    button = MDCButton('Favorite', icon='favorite', raised=True, reversed=True)
    container <= button
            

Outlined button
---------------

.. brython::

    from mdcframework.mdc import MDCButton
    #!ignore
    
    button = MDCButton('Outlined Button', raised=False, outlined=True)
    container <= button
            

Unelevated button
-----------------

.. brython::

    from mdcframework.mdc import MDCButton
    #!ignore
    
    button = MDCButton('Unelevated Button', raised=False, unelevated=True)
    container <= button
            

Icon button 
------------

.. brython::

    from mdcframework.mdc import MDCButton
    #!ignore
    
    button = MDCButton(icon='favorite', raised=False)
    container <= button
            
