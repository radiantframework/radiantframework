Theme
=====


MDL
---

You must use `Material Design <https://material.google.com/>`_ in your web app, **Django for Android** provide that through `MDL <https://getmdl.io/index.html>`_, you can too download your `custom template <https://getmdl.io/customize/index.html>`_.

For default MDL just add this to your template.

.. code-block:: html

    {% load staticfiles %}

    ...

    <link rel="stylesheet" href="{% static "djangoforandroid/mdl/mdl-1.1.3/material.min.css" %}"
    <script src="{% static "djangoforandroid/mdl/mdl-1.1.3/material.min.js" %}"></script>

Take a look for all `components <https://getmdl.io/components/index.html>`_ that you can use.


Others
------
There are many others alternatives, not only MDL for add **Material Design**

- `Materialize <http://materializecss.com/>`_
- `Material-UI <http://www.material-ui.com/#/>`_
- `MUI <https://www.muicss.com/>`_



