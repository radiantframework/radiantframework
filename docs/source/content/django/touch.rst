Touch support
=============



Hammer.js
---------

Users can include any libraries for support touch events, **Django for Android** include one of them: `Hammer.js <http://hammerjs.github.io>`_,
just add this to your template.

.. code-block:: html

    {% load staticfiles %}

    ...

    <script src="{% static "djangoforandroid/hammer/hammer-2.0.8.min.js" %}"></script>
    <script src="{% static "djangoforandroid/scripts/touchdrawer.js" %}"></script>

Above code add **hammer.js** and **touchdrawer.js** for touch interactions with the drawer.

Feel free to add your `custom events <http://hammerjs.github.io/getting-started/>`_.



Others
------
There are many others alternatives, not only `Hammer.js` for add touch events.

- `Touche.js <http://benhowdle.im/touche/>`_
- `jQuery UI Touch Punch <http://touchpunch.furf.com/>`_
- `QuoJS <http://quojs.tapquo.com/>`_

