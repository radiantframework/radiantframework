.. Radiant documentation master file, created by
   sphinx-quickstart on Tue Jun  5 13:31:58 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Radiant: Python framework for Android apps development
======================================================



.. toctree::
   :maxdepth: 2

   content/gettingstarted
   content/django/django_framework
   content/brython/brython_framework
   content/brython/components
   content/brython/autobuild



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


