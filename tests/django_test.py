import unittest
import shutil
import os
import sys

from radiant import projects

########################################################################
class DjangoTestCase(unittest.TestCase):
    """"""
    cwd = os.path.join(os.path.dirname(__file__), 'sandbox')


    #----------------------------------------------------------------------
    def test_01_create_project(self):
        """"""
        if os.path.exists(self.cwd):
            shutil.rmtree(self.cwd)
        os.mkdir(self.cwd)
        os.chdir(self.cwd)
        projects.create_project('startproject', 'django', 'django_test', 'My Django App', compilestatic=False, collectstatic=False)

        file = open(os.path.join('django_test','settings.py'), 'r')
        content = file.read()
        file.close()

        content = content.replace('/path/to/','/home/yeison/Development/android/')

        file = open(os.path.join('django_test','settings.py'), 'w')
        file.write(content)
        file.close()

        self.assertTrue(os.path.exists(os.path.join(self.cwd, 'django_test')))
        self.assertTrue(os.path.exists(os.path.join(self.cwd, 'django_test', 'manage.py')))
        self.assertFalse(os.path.exists(os.path.join(self.cwd, 'django_test', 'resources')))
        self.assertFalse(os.path.exists(os.path.join(self.cwd, 'django_test', 'resources', 'COMPILED')))


    # #----------------------------------------------------------------------
    # def test_02_brython_test(self):
        # """"""
        # os.chdir(os.path.join(self.cwd, 'brython_test'))
        # os.system("python manage.py androidapk --brython_test")

        # self.assertTrue(os.path.exists(os.path.join(self.cwd, 'brython_test', 'brython_app')))


    #----------------------------------------------------------------------
    def test_03_collectstatic(self):
        """"""
        os.chdir(os.path.join(self.cwd, 'django_test'))
        os.system("python manage.py collectstatic --noinput")

        self.assertTrue(os.path.exists(os.path.join(self.cwd, 'django_test', 'resources')))
        self.assertFalse(os.path.exists(os.path.join(self.cwd, 'django_test', 'resources', 'COMPILED')))


    #----------------------------------------------------------------------
    def test_04_compilestatic(self):
        """"""
        os.chdir(os.path.join(self.cwd, 'django_test'))
        os.system("python manage.py compilestatic")

        self.assertTrue(os.path.exists(os.path.join(self.cwd, 'django_test', 'resources', 'COMPILED')))


    #----------------------------------------------------------------------
    def test_05_create(self):
        """"""
        os.chdir(os.path.join(self.cwd, 'django_test'))

        krt = os.path.join(os.path.expanduser('~/.radiant'), 'django_test')
        if os.path.exists(krt):
            shutil.rmtree(krt)

        os.system("python manage.py androidcreate --clean")
        self.assertTrue(os.path.exists(krt))


    #----------------------------------------------------------------------
    def test_06_apk_debug(self):
        """"""
        os.chdir(os.path.join(self.cwd, 'django_test'))
        os.system("python manage.py androidapk --debug")

        self.assertTrue(os.path.exists(os.path.join(self.cwd, 'brython_test', 'MyDjangoApp-1.0-debug.apk')))


    #----------------------------------------------------------------------
    def test_07_apk_release(self):
        """"""
        os.chdir(os.path.join(self.cwd, 'django_test'))
        shutil.copyfile(os.path.join(os.path.dirname(__file__), 'keys', 'radiant.upload.jks'), os.path.join(self.cwd, 'django_test', 'radiant.upload.jks'))
        os.system("python manage.py androidapk --release")

        self.assertTrue(os.path.exists(os.path.join(self.cwd, 'django_test', 'MyDjangoApp-1.0-release.apk')))


if __name__ == '__main__':
    unittest.main()
