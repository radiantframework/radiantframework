import unittest
import shutil
import os
import sys
import webbrowser
# from urllib.request import urlopen
import subprocess
import time

from radiant import projects

########################################################################
class BrythonTestCase(unittest.TestCase):
    """"""
    cwd = os.path.join(os.path.dirname(__file__), 'sandbox')


    #----------------------------------------------------------------------
    def test_01_create_project(self):
        """"""
        if os.path.exists(self.cwd):
            shutil.rmtree(self.cwd)
        os.mkdir(self.cwd)
        os.chdir(self.cwd)
        projects.create_project('startproject', 'brython', 'brython_test', 'My Brython App', compilestatic=False, collectstatic=False)

        file = open('settings.py', 'r')
        content = file.read()
        file.close()

        content = content.replace('/path/to/','/home/yeison/Development/android/')

        file = open('settings.py', 'w')
        file.write(content)
        file.close()

        self.assertTrue(os.path.exists(os.path.join(self.cwd, 'brython_test')))
        self.assertTrue(os.path.exists(os.path.join(self.cwd, 'brython_test', 'manage.py')))
        self.assertFalse(os.path.exists(os.path.join(self.cwd, 'brython_test', 'resources')))
        self.assertFalse(os.path.exists(os.path.join(self.cwd, 'brython_test', 'resources', 'COMPILED')))


    # #----------------------------------------------------------------------
    # def test_02_brython_test(self):
        # """"""
        # os.chdir(os.path.join(self.cwd, 'brython_test'))
        # os.system("python manage.py androidapk --brython_test")

        # self.assertTrue(os.path.exists(os.path.join(self.cwd, 'brython_test', 'brython_app')))


    #----------------------------------------------------------------------
    def test_03_collectstatic(self):
        """"""
        os.chdir(os.path.join(self.cwd, 'brython_test'))
        os.system("python manage.py collectstatic --noinput")

        self.assertTrue(os.path.exists(os.path.join(self.cwd, 'brython_test', 'resources')))
        self.assertFalse(os.path.exists(os.path.join(self.cwd, 'brython_test', 'resources', 'COMPILED')))


    #----------------------------------------------------------------------
    def test_04_compilestatic(self):
        """"""
        os.chdir(os.path.join(self.cwd, 'brython_test'))
        os.system("python manage.py compilestatic")

        self.assertTrue(os.path.exists(os.path.join(self.cwd, 'brython_test', 'resources', 'COMPILED')))


    #----------------------------------------------------------------------
    def test_041_runserver(self):
        """"""
        os.chdir(os.path.join(self.cwd, 'brython_test'))
        # os.system("python manage.py runserver 0.0.0.0:8800 &&")

        p = subprocess.Popen(['python', 'manage.py', 'runserver', '0.0.0.0:8800'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        time.sleep(5)

        for page in ['buttons']:
            webbrowser.get('chromium %s').open_new_tab('http://0.0.0.0:8800#{}'.format(page))

        shutil.copytree('docs', '../../../docs/source/content/brython/mdc_components/examples')


        # self.assertTrue(os.path.exists(os.path.join(self.cwd, 'brython_test', 'brython_app')))


    # #----------------------------------------------------------------------
    # def test_05_create(self):
        # """"""
        # os.chdir(os.path.join(self.cwd, 'brython_test'))

        # krt = os.path.join(os.path.expanduser('~/.radiant'), 'brython_test')
        # if os.path.exists(krt):
            # shutil.rmtree(krt)

        # os.system("python manage.py androidcreate --clean")
        # self.assertTrue(os.path.exists(krt))


    # #----------------------------------------------------------------------
    # def test_06_apk_debug(self):
        # """"""
        # os.chdir(os.path.join(self.cwd, 'brython_test'))
        # os.system("python manage.py androidapk --debug")

        # self.assertTrue(os.path.exists(os.path.join(self.cwd, 'brython_test', 'MyBrythonApp-1.0-debug.apk')))


    # #----------------------------------------------------------------------
    # def test_07_apk_release(self):
        # """"""
        # os.chdir(os.path.join(self.cwd, 'brython_test'))
        # shutil.copyfile(os.path.join(os.path.dirname(__file__), 'keys', 'radiant.upload.jks'), os.path.join(self.cwd, 'brython_test', 'radiant.upload.jks'))
        # os.system("python manage.py androidapk --release")

        # self.assertTrue(os.path.exists(os.path.join(self.cwd, 'brython_test', 'MyBrythonApp-1.0-release.apk')))


if __name__ == '__main__':
    unittest.main()
