"""
Django settings for {{PROJECT}} project.

Generated by 'django-admin startproject' using Django 2.0.4.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '5l!8b@e6h&5+z5^rkgj+ztvzr+ua02^0iyt$!=3h$gd#9nf%b9'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'static_precompiler',

    'radiant.builder',
    'radiant.framework',
,
    'filemanager.apps.FilemanagerConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = '{{PROJECT}}.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': True,
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = '{{PROJECT}}.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'resources')

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

APPEND_SLASH = True

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # other finders..
    'static_precompiler.finders.StaticPrecompilerFinder',
)


STATIC_PRECOMPILER_COMPILERS = (
    'static_precompiler.compilers.CoffeeScript',
    'static_precompiler.compilers.SASS',
    'static_precompiler.compilers.SCSS',
)

# Uncomment before to create the APK
STATIC_PRECOMPILER_DISABLE_AUTO_COMPILE = True

ANDROID = {

    'APK': {
        'name': "{{APPNAME}}",
        'version': '1.0',
        # 'numericversion': '1',
        'package': 'com.radiant.{{PACKAGENAME}}',
        'icon': os.path.join(BASE_DIR, 'static', 'images', 'icon.png'),
        'statusbarcolor': '#00675b',
        'navigationbarcolor': '#00675b',
        'orientation': 'portrait',
    },

    'APP': {
        'multithread': False,
        #'logs': '/storage/emulated/0',
    },

    'BRYTHON': False,

    'ANDROID': {
        'ARCH': 'armeabi-v7a',
        'SDK': '/path/to/sdk-tools-linux',
        'BUILD_TOOL': 'ant',
        'API': '26',
        'CRYSTAX_NDK': '/path/to/crystax-ndk-10.3.2',
        'CRYSTAX_NDK_VERSION': '10.3.2',
    },

    'KEY': {
        'RELEASE_KEYSTORE': os.path.join(BASE_DIR, 'radiant.upload.jks'),
        'RELEASE_KEYALIAS': 'radiant',
        'RELEASE_KEYSTORE_PASSWD': 'radiant', #use your own password
        'RELEASE_KEYALIAS_PASSWD': 'radiant',
    },

    'SPLASH': {
        'static_html': os.path.join(BASE_DIR, 'templates', 'splash.html'),
        'resources': [os.path.join(BASE_DIR, 'static', 'images', 'icon.png'),
                      os.path.join(BASE_DIR, 'static', 'images', 'splash.svg'),
                     ],
    },

    'PORT': '1248',
    'IP': 'localhost',

    'BUILD': {
        'exclude_dirs': ['radiant', '.hg'],
        'include_exts': ['py', 'png', 'sqlite3', 'html', 'css', 'js', 'svg', 'ttf', 'eot', 'woff', 'woff2', 'otf', 'xml'],
        },

    'THEME': {
        'colors': os.path.join(BASE_DIR, 'colors.xml'),
    },

}